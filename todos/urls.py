from django.urls import path
from .views import todo_list_view, todo_list_details


urlpatterns = [
    path("", todo_list_view, name="todo_list_list"),
    path("<int:id>/", todo_list_details, name="todo_list_details"),
]

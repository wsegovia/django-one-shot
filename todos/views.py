from django.shortcuts import render
from .models import TodoList, TodoItem


def todo_list_view(request):
    todo_lists = TodoList.objects.all()
    context = {
        'todo_lists': todo_lists
    }
    return render(request, 'todo_list.html', context)


def todo_list_details(request, id):
    todo_list = TodoList.objects.get(id=id)
    items = todo_list.items.all()
    context = {
        "todo_list": todo_list,
        "items": items,
    }
    return render(request, "details.html", context)
